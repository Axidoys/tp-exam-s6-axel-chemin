/*
 * CHEMIN Axel
 */

#ifndef IO_H_
#define IO_H_

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

#define RED "\x1B[31m"
#define GRN "\x1B[32m"
#define YEL "\x1B[33m"
#define BLU "\x1B[34m"
#define MAG "\x1B[35m"
#define CYN "\x1B[36m"
#define WHT "\x1B[37m"
#define RESET "\x1B[0m"

#define MAXCMDSIZE 50
#define DATESIZE 30

void printError(char *errStr);
void printHelpRun(char *prgName);

#endif
