BIN = bin
OBJ = obj
SRC = src
INCLUDE = include

CFLAGS = -W -Wall -g -O0

GNUMAKEFLAGS = --no-print-directory


all: dirs release

dirs: obj bin

release: main
	cp ${BIN}/$^.out $^

main : ${OBJ}/main.o ${OBJ}/trees.o ${OBJ}/io.o
	gcc -o ${BIN}/$@.out $^ ${CFLAGS}

${OBJ}/io.o : ${SRC}/io.c
	gcc -I ${INCLUDE} -o $@ -c $^ ${CFLAGS}

${OBJ}/trees.o : ${SRC}/trees.c
	gcc -I ${INCLUDE} -o $@ -c $^ ${CFLAGS}

${OBJ}/main.o : ${SRC}/main.c
	gcc -I ${INCLUDE} -o $@ -c $^ ${CFLAGS}


obj :
	@-mkdir ${OBJ}

bin :
	@-mkdir ${BIN}

clean :
	rm -rf obj/*.o

mrproper: clean
	rm -rf bin
	rm -rf obj
	rm -rf ./main
