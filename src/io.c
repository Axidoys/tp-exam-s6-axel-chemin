/*
 * CHEMIN Axel
 */

#include "io.h"

void printError(char *errStr) {
  printf(RED "Error :" YEL " '%s'\n" RESET, errStr);
}

void printHelpRun(char *prgName) {
  printf(GRN "Help :\n" RESET);
  printf("  TP exam Axel CHEMIN : ");
  printf("        %s <data dir path>\n", prgName);
}
