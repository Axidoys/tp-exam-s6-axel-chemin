/*
 * CHEMIN Axel
 */

#include "trees.h"

// construction of a tree by pointer
void cons_tree(struct node **ptr_tree, int val, struct node *left,
               struct node *right) {
  if (*ptr_tree == NULL) {
    *ptr_tree = malloc(sizeof(struct node));
    (*ptr_tree)->val = val;
    (*ptr_tree)->left = left;
    (*ptr_tree)->right = right;
  }
}

// initialize un empty tree
void mk_empty_tree(struct node **ptr_tree) { *ptr_tree = NULL; }

// is tree empty?
bool is_empty(struct node *tree) { return tree == NULL; }

// is tree a leaf?
bool is_leaf(struct node *tree) {
  return (!is_empty(tree) && is_empty(tree->left) && is_empty(tree->right));
}

// add x in a bst wtr its value.
void add(struct node **ptr_tree, int x) {
  if (is_empty(*ptr_tree))
    cons_tree(ptr_tree, x, NULL, NULL);
  else {
    if (x <= (*ptr_tree)->val)
      add(&(*ptr_tree)->left, x);
    else {
      add(&(*ptr_tree)->right, x);
    }
  }
}

// print values of tree in ascendant order
void print_tree(struct node *tree) {
  if (!(is_empty(tree))) {
    print_tree(tree->right);
    printf("%d ", tree->val);
    print_tree(tree->left);
  }
}

// build a tree "add"ing values of the file fp
void load_tree(FILE *fp, struct node **ptr_tree) {
  int nb;
  while (fscanf(fp, "%d", &nb) != EOF) {
    add(ptr_tree, nb);
  }
}

// Free all memory used by the tree
void free_tree(struct node **ptr_tree) {
  if (!is_empty(*ptr_tree)) {
    if (is_leaf(*ptr_tree))
      free(*ptr_tree);
    else {
      free_tree(&(**ptr_tree).left);
      free_tree(&(**ptr_tree).right);
    }
  }
}

void _height_tree(struct node *tree, int level,
                  int *depth) { // first call w/ level = -1, *depth = 0
  level++;
  if (!(is_empty(tree))) {
    _height_tree(tree->right, level, depth);
    _height_tree(tree->left, level, depth);
  }

  if (level > *depth)
    *depth = level;
}

int height_tree(struct node *tree) {
  int ret = 0;
  _height_tree(tree, -1, &ret);
  return ret;
}

int min_tree(struct node *tree) {
  if (is_empty(tree)) // is error?
    return 0;

  // car ordonné
  if (tree->left != NULL)
    return min_tree(tree->left);
  return tree->val;
}

int max_tree(struct node *tree) {
  if (is_empty(tree)) // is error?
    return 0;

  // car ordonné
  if (tree->right != NULL)
    return max_tree(tree->right);
  return tree->val;
}

// print values of tree in ascendant order
bool is_tree_odd(struct node *tree) {
  if (!(is_empty(tree))) {
    return (tree->val % 2 == 1) && is_tree_odd(tree->right) &&
           is_tree_odd(tree->left);
  }
  return true;
}

bool tree_search(struct node *tree, int x) {
  if (!(is_empty(tree))) {
    return (tree->val == x) || tree_search(tree->right, x) ||
           tree_search(tree->left, x);
  }
  return false;
}
