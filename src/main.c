/*
 * CHEMIN Axel
 */

#include "io.h"
#include "trees.h"
#include <stdio.h>

int main(int argc, char *argv[]) {
  printf("hello world !\n");

  if (argc < 2) {
    printError("Not enough argument");
    printHelpRun(argv[0]);
    exit(1);
  }

  struct node *tree;
  mk_empty_tree(&tree);

  FILE *f;
  f = fopen(argv[1], "r");
  // TODO: test error

  load_tree(f, &tree);

  print_tree(tree);
  printf("\n");

  // TESTS Q. 4

  printf("height of tree : %d\n", height_tree(tree));

  printf("min of tree : %d\n", min_tree(tree));
  printf("max of tree : %d\n", max_tree(tree));

  printf("%d is_tree_odd\n", is_tree_odd(tree));

  printf("%d tree_search(tree, 42)\n", tree_search(tree, 42));
  printf("%d tree_search(tree, 43)\n", tree_search(tree, 43));

  // FIN Q.4

  free_tree(&tree);
  fclose(f);

  return EXIT_SUCCESS;
}
